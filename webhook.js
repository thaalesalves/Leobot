const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const fb = require('./fb');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const server = app.listen(process.env.PORT || 9669, () => {
  console.log(
    'Servidor iniciado na porta %d em modo %s',
    server.address().port,
    app.settings.env
  );
});

app.get('/webhook', (req, res) => {
  if (req.query['hub.mode'] && req.query['hub.verify_token'] === 'tuxedo_cat') {
    res.status(200).send(req.query['hub.challenge']);
  } else {
    res.status(403).end();
  }
});

app.post('/webhook', (req, res) => {
  if (req.body.object === 'page') {
    req.body.entry.forEach(entry => {
      entry.messaging.forEach(event => {
        if (event.message && event.message.text) {
          fb.sendMessage(event, event.sender.id, event.message.text);
        }
      });
    });
    res.status(200).end();
  }
});
