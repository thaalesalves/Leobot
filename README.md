# Leobot
Chatbot para Facebook desenvolvido para a Atlética de Tecnologia da Informação da Universidade de Mogi das Cruzes. Será utilizado pelo Messenger para que os alunos tenham acesso a informações do curso, eventos, parceiros e descontos.

## Live demo
Por ora, o bot está em fase de desenvolvimento, então somente membros do app poderão testá-lo na página da Atlética. But fork it! Crie um aplicativo próprio no Facebook e vincule a uma página, e será muito fácil testá-lo!

## Como utilizar
É necessário ter o NodeJS 9.x instalado para rodar o projeto. Por padrão, ele inicia na porta **9669**, mas isso pode ser alterado no `webhook.js`. Para integra-lo com o Facebook, é necessário criar um app e configurar o webhook para a receber requisições o endereço do servidor, e, por padrão do Facebook, é necessário ter SSL. Você pode usar seu próprio certificado ou usar serviços como o **[https://ngrok.com/](ngrok)** como proxy para criar um túnel para seu servidor (é a alternativa que uso). Tendo tudo isso pronto, basta rodar o `node webhook.js` e *voilà*!

# Especificações técnicas
## Tecnologias utilizadas
* NodeJS
* ngrok
* express
* body-parser
* Facebook
* Dialogflow (antigo api.ai)
