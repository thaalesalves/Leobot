const request = require("request");
const apiai = require("apiai");
const apiaiApp = apiai("36ba562135cc48a894a0f27a07861d70", {
  language: "pt-BR"
});

const accesToken =
  "EAAMZBre7sJaUBAEdAf3LdggdBySBbxde3mOmos7HzLMMh7Bnx1hThzosZC4Dm6XS986JfB6bCqHsDPROa3Mzc3vGnqHKgZC52phjByXSQL0aQZAd3U8GZBHfzKReV9HYo9PxJpbLZACA6RipDX1ouvZBqv8PnyOq1MOsJ9734nZCDAZDZD";

const fb = {
  sendMessage(event, sender, text) {
    console.log("Recebi uma mensagem de " + sender + ": " + text);

    const apiai = apiaiApp.textRequest(text, {
      sessionId: sender
    });

    apiai.on("response", response => {
      const aiText = response.result.fulfillment.speech;
      console.log("Mensagem a ser enviada: " + aiText);

      request(
        {
          url: "https://graph.facebook.com/v2.6/me/messages",
          qs: {
            access_token: accesToken
          },
          method: "POST",
          json: {
            recipient: { id: sender },
            message: { text: aiText } //aiText
          }
        },
        (error, response) => {
          if (error) {
            console.log("Erro ao enviar mensagem: ", error);
          } else if (response.body.error) {
            console.log("Erro: ", response.body.error);
          }
        }
      );
    });

    apiai.on("error", error => {
      console.log(error);
    });

    apiai.end();
  }
};

module.exports = fb;
